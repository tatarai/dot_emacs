; -*- Emacs-Lisp -*-
; (setq author "Tatsuhiko Arai")

;(setq url-proxy-services
;      '(("http"  . "proxy.example.com:8080")
;        ("https" . "proxy.example.com:8080")))

; ------------------
; Default Major Mode
; ------------------
(setq default-major-mode 'lisp-interaction-mode)

; -----------------
; Default Directory
; -----------------
(setq default-directory "~/")

; ---------
; Load Path
; ---------
(defun add-to-load-path (&rest paths)
  (let (path)
    (dolist (path paths paths)
      (let ((default-directory
              (expand-file-name (concat user-emacs-directory path))))
        (add-to-list 'load-path default-directory)
        (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
            (normal-top-level-add-subdirs-to-load-path))))))
(add-to-load-path "elisp" "auto-install" "conf")

;; ---------------------------------
;; Emacs Lisp Package Archive (ELPA)
;; ---------------------------------
;;; This was installed by package-install.el.
;;; This provides support for the package system and
;;; interfacing with ELPA, the package archive.
;;; Move this code earlier if you want to reference
;;; packages in your .emacs.
(when (require 'package nil t)
  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
  ;;  (add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
  (package-initialize))

; ------------
; Auto Install
; ------------
; (package-install 'auto-install)
(require 'auto-install)
; EmacsWikiに登録されているelispの名前を取得する
(auto-install-update-emacswiki-package-name t)
(auto-install-compatibility-setup)

; ------------------------------------
; Automagic emacs lisp code annotation
; ------------------------------------
; (package-install 'lispxmp)
(require 'lispxmp)

; ----------------------------------
; minor mode for editing parentheses
; ----------------------------------
; (package-install 'paredit)
(require 'paredit)

; -------------
; Shell History
; -------------
; (package-install 'shell-history)
(require 'shell-history)

; ----------
; Multi Term
; ----------
; (package-install 'multi-term)
(require 'multi-term)
(setq multi-term-program "/bin/bash")

; -----
; Theme
; -----
; (package-install 'zenburn-theme)
(load-theme 'zenburn t)

; -----
; Ediff
; -----
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

; -----------------------
; System related settings
; -----------------------
; sudo apt-get install anthy.el
(when (eq system-type 'gnu/linux)
  (set-language-environment "Japanese")
  (load-library "anthy")
  (set-input-method 'japanese-anthy))

(when (eq system-type 'darwin)
  (set-face-attribute 'default nil
                      :family "Menlo"
                      :height 150)
  (setq default-input-method "MacOSX")
  (setq ns-command-modifier (quote meta)))

(when (eq system-type 'windows-nt)
  (set-face-attribute 'default nil
                      :family "Lucida Console"
                      :height 110))

; ------------
; Key Bindings
; ------------
(global-set-key "\C-c\M-w" 'clipboard-kill-ring-save) ; <copy>
(global-set-key "\C-c\C-w" 'clipboard-kill-region)    ; <cut>
(global-set-key "\C-c\C-y" 'clipboard-yank)           ; <paste>
(global-set-key "\C-c\C-f" 'find-lisp-find-dired) ; Find files in DIR, matching REGEXP.
(global-set-key "\C-c\C-g" 'grep-find)            ; Run grep via find

; -----
; gtags
; -----
; sudo apt-get install global
; /usr/share/emacs/site-lisp/global/gtags.el
; gtags --gtagsconf=/usr/local/etc/gtags.conf --gtagslabel=pygments [--debug]
(when (require 'gtags nil t)
  (setq gtags-mode-hook
        '(lambda ()
           ;; 指定したファイルを探す
           (local-set-key "\M-p" 'gtags-find-pattern)
           ;; 指定したパターンを探す
           (local-set-key "\M-g" 'gtags-find-with-grep)
           ;; 指定した関数が定義されている部分を探す
           (local-set-key "\M-t" 'gtags-find-tag)
           ;; 指定した変数、定義の定義元を探す
           (local-set-key "\M-r" 'gtags-find-rtag)
           ;; 指定した関数が参照されている部分を探す
           (local-set-key "\M-s" 'gtags-find-symbol)
           ;; gtagsでジャンプする一つ前の状態に戻る
           (local-set-key "\C-t" 'gtags-pop-stack)))
  (setq gtags-suggested-key-mapping t)
  (setq gtags-pop-delete t)
  (setq gtags-path-style 'relative))

; -----
; ctags
; -----
; (package-install 'ctags)
(require 'ctags nil t)
(setq tags-revert-without-query t)
(setq ctags-command "ctags-exuberant -a -e -f TAGS --tag-relative -R")
(global-set-key (kbd "<f6>") 'ctags-create-or-update-tags-table)

; -------------------
; Emacs Got Git (Egg)
; -------------------
; (package-install 'egg)
(when (executable-find "git")
  (require 'egg))

; -------------
; Switch Buffer
; -------------
(iswitchb-mode 1)
(setq read-buffer-function 'iswitchb-read-buffer)
(setq iswitchb-regexp nil)

; -----
; Frame
; -----
(setq default-frame-alist
      (append (list
                                        ; Disable scroll-bar
               '(vertical-scroll-bars . nil)
                                        ; left-top position
               '(top . -1)              ; y座標(下隅)
               '(left . -1)             ; x座標(右隅)
                                        ; display size
                                        ;'(width . 80)
               '(height . 36)
               )
              default-frame-alist))
;; title bar にファイル名を表示
(setq frame-title-format "%f")
;; tool bar を表示させない
(tool-bar-mode 0)
;; menu bar を表示させない
;(menu-bar-mode -1)
;; スプラッシュを非表示にする
(setq inhibit-startup-message t)

;; 行番号・桁番号を modeline に表示する
(line-number-mode t)   ; 行番号
(column-number-mode t) ; 桁番号

;; tab ではなく space を使う
(setq-default indent-tabs-mode nil)
;; tab 幅を 4 に設定
(setq-default tab-width 4)
;; バッファの最後の行で next-line しても新しい行を作らない
;(setq next-line-add-newlines nil)
;; narrowing を禁止
;(put 'narrow-to-region 'disabled nil)
;; 対応する括弧を光らせる。
(show-paren-mode 1)

; ----
; ffap
; ----
(ffap-bindings)

; -------
; tempbuf
; -------
; (package-install 'tempbuf)
(require 'tempbuf)
; ファイルを開いたら自動的にtempbufを有効にする。
(add-hook 'find-file-hooks 'turn-on-tempbuf-mode)
; diredバッファに対してtempbufを有効にする。
(add-hook 'dired-mode-hook 'turn-on-tempbuf-mode)

; -----------
; recentf-ext
; -----------
; (package-install 'recentf-ext)
(require 'recentf-ext)
; 最近使ったファイル500個を保存
(setq recentf-max-saved-items 500)
; 最近使ったファイルに登録しないファイルを指定
(setq recentf-exclude '("/TAGS$" "/var/tmp/"))
(global-set-key [f5] 'recentf-open-files)
;(global-set-key [f6] 'kill-buffer)

; ---------
; text-mode
; ---------
(add-hook 'text-mode-hook
          '(lambda ()
             (progn
;;; 76文字幅でオートインデント
               (set-fill-column 76)
               )))

; ----------------
; c-mode, c++-mode
; ----------------
(add-hook 'c-mode-common-hook
          '(lambda ()
             ;; K&Rスタイル
             (c-set-style "k&r")
             ; インデント幅
             (setq c-basic-offset 4)
             ; gtags-mode on
             (gtags-mode 1)
             ))

;(setq auto-mode-alist
;      ;;; 拡張子とモードの対応
;      (append
;       '(("\\.c$" . c-mode))
;       '(("\\.h$" . c-mode))
;       '(("\\.cpp$" . c++-mode))
;       auto-mode-alist))

; ---------
; flex-mode
; ---------
; (install-elisp "http://ftp.sunet.se/pub/gnu/emacs-lisp/incoming/flex-mode.el")
(require 'flex-mode)
(add-to-list 'auto-mode-alist '("\\.l$" . flex-mode))

; --------
; php-mode
; --------
; (package-install 'php-mode)
(require 'php-mode)
(setq php-mode-force-pear t)
(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))

; -------------
; markdown-mode
; ------------
; (package-install 'markdown-mode)
(require 'markdown-mode)
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown$" . markdown-mode))

; ---------
; ruby-mode
; ---------
; (package-install 'ruby-electric)
; (package-install 'inf-ruby)
; (package-install 'ruby-block)
; 括弧の自動挿入
(require 'ruby-electric)
; endに対応する行のハイライト
(require 'ruby-block)
(setq ruby-block-highlight-toggle t)
; インタラクティブRubyを利用する
(require 'inf-ruby)
(autoload 'run-ruby "inf-ruby"
"Run an inferior Ruby process")
(autoload 'inf-ruby-keys "inf-ruby"
"Set local key defs for inf-ruby in ruby-mode")
;; ruby-mode-hook用の関数を定義
(defun ruby-mode-hooks ()
  (inf-ruby-keys)
  (ruby-electric-mode t)
  (ruby-block-mode t))
(add-hook 'ruby-mode-hook
          '(lambda() (gtags-mode 1))
          )
(add-hook 'ruby-mode-hook 'ruby-mode-hooks)
(add-to-list 'auto-mode-alist '("\\.ru$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.rake$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.builder$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.erb$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Gemfile$" . ruby-mode))
(add-hook 'ruby-mode-hook
          '(lambda () (rinari-launch)))

; --------
; js2-mode
; --------
; (package-install 'js2-mode)
(require 'js2-mode)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.json$" . js2-mode))

; ---------
; coffee-mode
; ---------
; (package-install 'coffee-mode)
(require 'coffee-mode)
(add-to-list 'auto-mode-alist
             '("\\.coffee$" . coffee-mode))
(add-hook 'coffee-mode-hook
          '(lambda () (setq tab-width 2)))
(add-hook 'coffee-mode-hook
          '(lambda () (rinari-launch)))

; ---------
; yaml-mode
; ---------
; (package-install 'yaml-mode)
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))

; ---------
; haml-mode
; ---------
; (package-install 'haml-mode)
(require 'haml-mode)
(add-to-list 'auto-mode-alist '("\\.haml$" . haml-mode))
(add-hook 'haml-mode-hook
          '(lambda () (rinari-launch)))

; ---------
; sass-mode
; ---------
; (package-install 'sass-mode)
(require 'sass-mode)
(add-to-list 'auto-mode-alist '("\\.\\(scss\\|css\\)\\'" . sass-mode))
(add-hook 'scss-mode-hook 'ac-css-mode-setup)
(add-hook 'scss-mode-hook
          '(lambda ()
;;; インデント幅
             (setq css-indent-offset 2)
             ))
(add-hook 'scss-mode-hook
          '(lambda () (rinari-launch)))

; -------------------------------------------
; Ruby on Rails Minor Mode for Emacs (Rinari)
; -------------------------------------------
; (package-install 'rinari)
; (package-install 'rhtml-mode)
(require 'ido)
(ido-mode t)
(require 'rinari)
(require 'rhtml-mode)
(add-hook 'rhtml-mode-hook
     	  '(lambda () (rinari-launch)))

; ----------
; swift-mode
; ----------
; (package-install 'swift-mode)
(require 'swift-mode)
(add-hook 'rhtml-mode-hook
          '(lambda () (rinari-launch)))
(setq rinari-tags-file-name "TAGS")

